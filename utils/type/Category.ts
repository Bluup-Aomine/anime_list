import DateTimeFormat = Intl.DateTimeFormat;

export type Categories = {
    id?: number,
    name?: string,
    slug?: string,
    content?: string,
    created_at?: DateTimeFormat,
    updated_at?: DateTimeFormat
}
