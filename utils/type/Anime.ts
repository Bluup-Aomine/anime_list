import DateTimeFormat = Intl.DateTimeFormat
import {Categories} from './Category'

export type DataAnime = {
    id?: number,
    title?: string
    slug?: string
    content?: string
    type?: string
    studios?: string
    status?: string
    start_date?: DateTimeFormat
    end_date?: DateTimeFormat
    created_at?: DateTimeFormat
    updated_at?: DateTimeFormat,
    category: Categories
}
