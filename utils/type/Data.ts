export type DataCategoryForm = {
    name: string
    slug: string
    content: string
}
