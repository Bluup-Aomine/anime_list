import Layout from '../components/Layout'
import React from "react";

export default function Login()
{
    return <Layout>
        {/* Breadcrumb */}
        <section className='breadcrumb_normal set_bg_img' style={{backgroundImage: `url('/normal-breadcrumb.jpg')`}}>
            <div className='container'>
                <div className="row">
                    <div className='col-lg-12 text-center'>
                        <div className='breadcrumb_normal_text'>
                            <h2>Login</h2>
                            <p>Welcome to the official Anime&nbsp;blog.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {/* Login Section */}
        <section className='login block'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6'>
                        <div className='login_form'>
                            <h3>Login</h3>
                            <form action="#">
                                <div className='input_item'>
                                    <input type="text" placeholder="Email address"/>
                                    <span className='icon_mail'/>
                                </div>
                                <div className='input_item'>
                                    <input type="text" placeholder="Password"/>
                                    <span className='icon_lock'/>
                                </div>
                                <button type='button' className='btn_red'>Login Now</button>
                            </form>
                            <a href="#" className='forget_password'>Forgot Your Password?</a>
                        </div>
                    </div>
                    <div className='col-lg-6'>
                        <div className='login_register'>
                            <h3>Dont’t Have An Account?</h3>
                            <a href="#" className="primary_btn">Register Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </Layout>
}
