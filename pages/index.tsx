import React, {useEffect, useState} from 'react'
import dynamic from 'next/dynamic'
// Component
import Layout from '../components/Layout'
import HeaderSection from '../components/anime/HeaderSection'
import {AnimeItem, AnimeItemSidebar} from 'components/anime/AnimeItem'
// OwlCarousel
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
// FontAwesome

const OwlCarousel = dynamic(() => import('@ntegral/react-owl-carousel'), {
    ssr: false
});

export default function Home() {
    const navText = ['<span class=\'arrow_carrot-left\'></span>', '<span class=\'arrow_carrot-right\'></span>']
    const [animeTrending, setAnimeTrending] = useState([0, 1, 2, 3, 4, 5])
    const [animeItemSidebar, setAnimeItemSidebar] = useState([0, 1, 2, 3, 4])

    useEffect(() => {
    }, [])

    return <Layout>
        {/* Hero Section */}
        <section className='hero'>
            <div className='container'>
                <OwlCarousel className='hero_slider' loop={true} dots={true} nav={true} margin={0} items={1}
                             navText={navText} animateOut='fadeOut' animateIn='fadeIn' smartSpeed={1200} autoplay={true}
                             mouseDrag={false}>
                    <div className='hero_items set_bg_img' style={{backgroundImage: `url('./hero-1.jpg')`}}>
                        <div className='row'>
                            <div className='col-lg-6'>
                                <div className='hero_text'>
                                    <div className='label'>Adventure</div>
                                    <h2>Fate / Stay Night: Unlimited Blade Works</h2>
                                    <p>After 30 days of travel across the world...</p>
                                    <a href="#">
                                        <span>More</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </OwlCarousel>
            </div>
        </section>
        {/* Anime Section */}
        <section className='anime block'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-8'>
                        <div className='trending_anime'>
                            <HeaderSection title='Trending Now'/>
                            <div className='row'>
                                {
                                    animeTrending.map((anime, index) => <AnimeItem key={index + 1}/>)
                                }
                            </div>
                        </div>
                        <div className='popular_anime'>
                            <HeaderSection title='Popular Shows'/>
                            <div className='row'>
                                {
                                    animeTrending.map((anime, index) => <AnimeItem key={index + 1}/>)
                                }
                            </div>
                        </div>
                        <div className='recent_anime'>
                            <HeaderSection title='Recently Added Shows'/>
                            <div className='row'>
                                {
                                    animeTrending.map((anime, index) => <AnimeItem key={index + 1}/>)
                                }
                            </div>
                        </div>
                        <div className='live_anime'>
                            <HeaderSection title='Live Action'/>
                            <div className='row'>
                                {
                                    animeTrending.map((anime, index) => <AnimeItem key={index + 1}/>)
                                }
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-4 col-md-6 col-sm-8'>
                        <div className='anime_sidebar'>
                            <div className='anime_sidebar_view'>
                                <div className='section-title'>
                                    <h5>
                                        Top Views
                                    </h5>
                                </div>
                                <ul className='filter_anime'>
                                    <li className='active'>Day</li>
                                    <li>Week</li>
                                    <li>Month</li>
                                    <li>Years</li>
                                </ul>
                                <div className='filter_anime_gallery'>
                                    {animeItemSidebar.map((anime, index) => <AnimeItemSidebar key={index + 1}/>)}
                                </div>
                            </div>
                            <div className='anime_sidebar_view'>
                                <div className='section-title'>
                                    <h5>
                                        New Comment
                                    </h5>
                                </div>
                                <div className='filter_anime_gallery'>
                                    {animeItemSidebar.map((anime, index) => <AnimeItemSidebar key={index + 1}/>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </Layout>
}
