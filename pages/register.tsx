import React from 'react'
// Component
import Layout from 'components/Layout'

export default function Register()
{
    return <Layout>
        {/* Breadcrumb */}
        <section className='breadcrumb_normal set_bg_img' style={{backgroundImage: `url('/normal-breadcrumb.jpg')`}}>
            <div className='container'>
                <div className="row">
                    <div className='col-lg-12 text-center'>
                        <div className='breadcrumb_normal_text'>
                            <h2>Register</h2>
                            <p>Welcome to the official Anime&nbsp;blog.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {/* Register Section */}
        <section className='login block'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6'>
                        <div className='login_form'>
                            <h3>Register</h3>
                            <form action="#">
                                <div className='input_item'>
                                    <input type="text" placeholder="Email address"/>
                                    <span className='icon_mail'/>
                                </div>
                                <div className='input_item'>
                                    <input type="text" placeholder="Your name"/>
                                    <span className='icon_profile'/>
                                </div>
                                <div className='input_item'>
                                    <input type="text" placeholder="Password"/>
                                    <span className='icon_lock'/>
                                </div>
                                <button type='button' className='btn_red'>Register Now</button>
                            </form>
                            <h5>
                                Already have an account? <a href="#">Log In !</a>
                            </h5>
                        </div>
                    </div>
                    <div className='col-lg-6'>
                        <div className='login_register'>
                            <h3>Do Have An Account ?</h3>
                            <a href="#" className="primary_btn">Login Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </Layout>
}
