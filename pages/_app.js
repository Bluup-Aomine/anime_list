// Styles
import 'bootstrap/dist/css/bootstrap.css'
import 'styles/lib/elegant-icons/style.css'
import 'styles/app_admin.scss'
import 'styles/app.scss'
import 'react-datepicker/dist/react-datepicker.css'

const MyApp = ({Component, pageProps}) => {
  return <Component {...pageProps} />
}

export default MyApp
