// Component
import Layout from '../../components/admin/Layout'
import AnimeStatics from '../../components/admin/statics/AnimeStatics'

export default function DashboardAdmin()
{
    return <Layout>
        <div className='page_heading'>
            <h3>Dashboard Admin</h3>
        </div>
        <div className='page-content'>
            <section className='row'>
                <div className='col-12 col-lg-9'>
                    <div className='row'>
                        <AnimeStatics/>
                        <AnimeStatics/>
                        <AnimeStatics/>
                        <AnimeStatics/>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}
