import Layout from '../../../components/admin/Layout'
import PageHeading from '../../../components/admin/page/PageHeading'
import Form from '../../../components/admin/anime/Form'
import Anime from '../../../lib/api/Anime'
import Category from '../../../lib/api/Category'
import {Categories} from '../../../utils/type/Category'

type AnimeCreatePropsType = {
    categories: Categories[]
}

function AnimeCreate({categories}: AnimeCreatePropsType)
{
    const createAnime = async (data) => {
        console.log(data)
        const response = await new Anime().createAnime(data)
        if (response.status === 200) {
            console.log('SUCCESS !')
        }
    }

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Anime Create' subtitle='You are create a new anime'/>
            <section id="create_anime_form">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Form Anime</h4>
                    </div>
                    <div className='card-content'>
                        <div className='card-body'>
                            <Form onSubmit={createAnime} categories={categories}/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}

export async function getStaticProps(context)
{
    const request = await (new Category).getCategories()
    const categories = await request.data

    return {
        props: {categories}
    }
}

export default AnimeCreate


