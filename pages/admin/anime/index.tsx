// React
import React, {useEffect, useState} from 'react'
import Link from 'next/link'
// Component
import PageHeading from '../../../components/admin/page/PageHeading'
import Layout from '../../../components/admin/Layout'
// Type
import {DataAnime} from '../../../utils/type/Anime'
// LIB
import Anime from 'lib/api/Anime'

export default function CategoriesAdmin() {
    const [animes, setAnimes] = useState<Array<DataAnime>>([])

    useEffect(() => {
        const callback = async () => {
            const response = await new Anime().getAnimes()
            if (response.status === 200) {
                setAnimes(response.data)
            }
        }
        callback()
    }, [])

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Animes' subtitle='Listening to your animes'/>
            <section id="categories">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Listening to animes</h4>
                    </div>
                    <div className='card-content'>
                        <div className='table-responsive'>
                            <table className='table mb-0 table-lg'>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Content</th>
                                    <th>Type</th>
                                    <th>Studios</th>
                                    <th>Category</th>
                                    <th>StartDate</th>
                                    <th>EndDate</th>
                                    <th>CreatedAt</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {animes && animes.map((anime, index) => {
                                    return <tr key={index + 1}>
                                        <td>{index + 1}</td>
                                        <td className="text-bold-500">{anime.title}</td>
                                        <td>{anime.slug}</td>
                                        <td>{anime.content}</td>
                                        <td>{anime.type}</td>
                                        <td>{anime.studios}</td>
                                        <td>{anime.category.name}</td>
                                        <td>{anime.start_date}</td>
                                        <td>{anime.end_date}</td>
                                        <td>{anime.created_at}</td>
                                        <td>
                                            <Link href={`/admin/anime/edit/${anime.id}`}>
                                                <a className='btn btn-secondary rounded-pill'>Edit</a>
                                            </Link>
                                            <button className='btn btn-danger rounded-pill'>Delete</button>
                                        </td>
                                    </tr>
                                })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}
