import Layout from '../../../../components/admin/Layout'
import PageHeading from '../../../../components/admin/page/PageHeading'
import Form from '../../../../components/admin/anime/Form'
import Anime from '../../../../lib/api/Anime'
import Category from '../../../../lib/api/Category'
import {Categories} from '../../../../utils/type/Category'
import {DataAnime} from "../../../../utils/type/Anime";

type AnimeCreatePropsType = {
    categories: Categories[],
    anime: DataAnime
}

function AnimeUpdate({categories, anime}: AnimeCreatePropsType)
{
    const updateAnime = async (data) => {
         const response = await new Anime().updateAnime(data, anime.id)
         if (response.status === 200) {
             console.log('SUCCESS !')
         }
    }

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Anime Update' subtitle='You are updating your anime'/>
            <section id="create_anime_form">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Form Anime</h4>
                    </div>
                    <div className='card-content'>
                        <div className='card-body'>
                            <Form onSubmit={updateAnime} categories={categories} anime={anime}/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}

export async function getServerSideProps({query})
{
    // Get all categories
    const categoriesRequest = await (new Category).getCategories()
    const categories = await categoriesRequest.data
    // Get anime current
    const animeRequest = await (new Anime()).getAnime(query.id)
    const anime = await animeRequest.data

    return {
        props: {categories, anime}
    }
}

export default AnimeUpdate


