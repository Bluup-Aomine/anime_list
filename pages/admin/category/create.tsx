import Layout from '../../../components/admin/Layout'
import PageHeading from '../../../components/admin/page/PageHeading'
import Form from '../../../components/admin/category/Form'
import Category from '../../../lib/api/Category'

export default function CategoryCreate() {

    const createCategory = async (data) => {
        const response = await new Category().createCategory(data)
        if (response.status === 200) {
            console.log('SUCCESS !')
        }
    }

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Category Create' subtitle='You are create a new category'/>
            <section id="create_category_form">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Form Category</h4>
                    </div>
                    <div className='card-content'>
                        <div className='card-body'>
                            <Form onSubmit={createCategory}/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}
