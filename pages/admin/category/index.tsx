import Link from 'next/link'

import PageHeading from '../../../components/admin/page/PageHeading'
import Layout from '../../../components/admin/Layout'
import {useEffect, useState} from 'react';
import {Categories} from '../../../utils/type/Category';
import Category from '../../../lib/api/Category';

export default function CategoriesAdmin() {
    const [categories, setCategories] = useState<Array<Categories>>([])

    useEffect(() => {
        const callback = async () => {
            const response = await new Category().getCategories()
            if (response.status === 200) {
                setCategories(response.data)
            }
        }
        callback()
    }, [])

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Categories' subtitle='Listening to your categories'/>
            <section id="categories">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Listening to categories</h4>
                    </div>
                    <div className='card-content'>
                        <div className='table-responsive'>
                            <table className='table mb-0 table-lg'>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Content</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {categories && categories.map((category, index) => {
                                    return <tr key={index + 1}>
                                        <td>{index + 1}</td>
                                        <td className="text-bold-500">{category.name}</td>
                                        <td>{category.slug}</td>
                                        <td className="text-bold-500">{category.content}</td>
                                        <td>
                                            <Link href={`/admin/category/edit/${category.id}`}>
                                                <a className='btn btn-secondary rounded-pill'>Edit</a>
                                            </Link>
                                            <button className='btn btn-danger rounded-pill'>Delete</button>
                                        </td>
                                    </tr>
                                })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}
