import Layout from '../../../../components/admin/Layout'
import PageHeading from '../../../../components/admin/page/PageHeading'
import Category from '../../../../lib/api/Category'
import {Categories} from '../../../../utils/type/Category'
import Form from "../../../../components/admin/category/Form";

type CategoryUpdatePropsType = {
    category: Categories
}

function CategoryUpdate({category}: CategoryUpdatePropsType)
{
    const updateCategory = async (data) => {
        const response = await new Category().updateCategory(data, category.id)
        if (response.status === 200) {
            console.log('SUCCESS !')
        }
    }

    return <Layout>
        <div className='page_heading'>
            <PageHeading title='Category Update' subtitle='You are updating your category'/>
            <section id="edit_category_form">
                <div className='card'>
                    <div className='card-header'>
                        <h4 className="card-title">Form Category</h4>
                    </div>
                    <div className='card-content'>
                        <div className='card-body'>
                            <Form onSubmit={updateCategory} category={category}/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </Layout>
}

export async function getServerSideProps({query})
{
    // Get category current
    const categoryRequest = await (new Category()).getCategory(query.id)
    const category = await categoryRequest.data

    return {
        props: {category}
    }
}

export default CategoryUpdate


