import React, {useState} from 'react'
// Component
import Layout from '../../components/Layout'
import AnimeReview from '../../components/anime/review/AnimeReview'
import AnimeReviewForm from 'components/anime/review/AnimeReviewForm'
// FontAwesome
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faComments, faHeartbeat, faHome, faStar} from '@fortawesome/free-solid-svg-icons'
import {AnimeItemSidebar} from "../../components/anime/AnimeItem";

export default function AnimeDetails() {
    const [animeReviews, setAnimeReviews] = useState([0, 1, 2, 3, 4])
    const [animeLikes, setAnimeLikes] = useState([0, 1, 2, 3, 4])

    return <Layout>
        {/* Breadcrumb */}
        <div className='breadcrumb_option'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='breadcrumb_links'>
                            <a href="#">
                                <FontAwesomeIcon icon={faHome}/>
                                Home
                            </a>
                            <a href="#">Categories</a>
                            <span>Romance</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Anime Section */}
        <div className='anime_details block'>
            <div className='container'>
                <div className='anime_details_content'>
                    <div className='row'>
                        <div className='col-lg-3'>
                            <div className='anime_details_bg set_bg_img'
                                 style={{backgroundImage: `url('/details-pic.jpg')`}}>
                                <div className='comment'>
                                    <FontAwesomeIcon icon={faComments}/>
                                    15
                                </div>
                                <div className='like'>
                                    <FontAwesomeIcon icon={faStar}/>
                                    8
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-9'>
                            <div className='anime_details_text'>
                                <div className='anime_details_title'>
                                    <h3>Fate Stay Night: Unlimited Blade</h3>
                                    <span>フェイト／ステイナイト, Feito／sutei naito</span>
                                </div>
                                <div className='anime_details_rating'>
                                    <div className='rating'>
                                        <a href="#">
                                            <FontAwesomeIcon icon={faStar}/>
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon={faStar}/>
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon={faStar}/>
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon={faStar}/>
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon={faStar}/>
                                        </a>
                                    </div>
                                    <span>1.523 Votes</span>
                                </div>
                                <p>
                                    Every human inhabiting the world of Alcia is branded by a “Count” or a number
                                    written on
                                    their body. For Hina’s mother, her total drops to 0 and she’s pulled into the Abyss,
                                    never to be seen again. But her mother’s last words send Hina on a quest to find a
                                    legendary hero from the Waste War - the fabled Ace!
                                </p>
                                <div className='anime_details_widget'>
                                    <div className='row'>
                                        <div className='col-lg-6 col-md-6'>
                                            <ul>
                                                <li>
                                                    <span>Type:</span>
                                                    TV Series
                                                </li>
                                                <li>
                                                    <span>Studios:</span>
                                                    Lerche
                                                </li>
                                                <li>
                                                    <span>Date aired:</span>
                                                    Oct 02, 2019 to ?
                                                </li>
                                                <li>
                                                    <span>Status:</span>
                                                    Airing
                                                </li>
                                                <li>
                                                    <span>Genre:</span>
                                                    Action, Adventure, Fantasy, Magic
                                                </li>
                                            </ul>
                                        </div>
                                        <div className='col-lg-6 col-md-6'>
                                            <ul>
                                                <li>
                                                    <span>Scores:</span>
                                                    7.31 / 1,515
                                                </li>
                                                <li>
                                                    <span>Rating:</span>
                                                    8.5 / 161 times
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className='anime_details_btn'>
                                    <a href="#" className='add_list_btn'>
                                        <FontAwesomeIcon icon={faHeartbeat}/> Add list
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Anime Review */}
                <div className='row'>
                    <div className='col-lg-8 col-md-8'>
                        <div className='anime_details_review'>
                            <div className='section-title'>
                                <h5>Reviews</h5>
                            </div>
                            {animeReviews.map((anime, index) => <AnimeReview key={index + 1}/>)}
                        </div>
                        <div className='anime_details_form'>
                            <div className='section-title'>
                                <h5>Your Comment</h5>
                            </div>
                            <AnimeReviewForm/>
                        </div>
                    </div>
                    <div className='col-lg-4 col-md-4'>
                        <div className='anime_details_sidebar'>
                            <div className='section-title'>
                                <h5>You might like...</h5>
                            </div>
                            {animeLikes.map((anime, index) => <AnimeItemSidebar key={index + 1}/>)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
}
