import React, {useState} from 'react'
// Component
import Layout from '../../components/Layout'
import PaginationAnime from 'components/anime/PaginationAnime'
// FontAwesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faHome} from '@fortawesome/free-solid-svg-icons'
import {AnimeItem, AnimeItemCommentSidebar, AnimeItemSidebar} from "../../components/anime/AnimeItem";
// Lib

export default function Categories() {
    const [animeItems, setAnimeItems] = useState([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    const [animeItemSidebar, setAnimeItemSidebar] = useState([0, 1, 2, 3, 4])

    return <Layout>
        {/* Breadcrumb */}
        <div className='breadcrumb_option'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='breadcrumb_links'>
                            <a href="#">
                                <FontAwesomeIcon icon={faHome}/>
                                Home
                            </a>
                            <a href="#">Categories</a>
                            <span>Romance</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Anime category section */}
        <div className='anime_category_page block'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-8'>
                        <div className='anime_category_page_content'>
                            <div className='anime_category_page_title'>
                                <div className='row'>
                                    <div className='col-lg-8 col-md-8 col-sm-8'>
                                        <div className='section-title'>
                                            <h4>Action</h4>
                                        </div>
                                    </div>
                                    <div className='col-lg-4 col-md-4 col-sm-6'>
                                        <div className='anime_category_page_filter'>
                                            <p>Order by:</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='row'>
                                {animeItems.map((anime, index) => <AnimeItem key={index + 1}/>)}
                            </div>
                        </div>
                        <PaginationAnime/>
                    </div>
                    <div className='col-lg-4 col-md-6 col-sm-8'>
                        <div className='anime_sidebar'>
                            <div className='anime_sidebar_view'>
                                <div className='section-title'>
                                    <h5>
                                        Top Views
                                    </h5>
                                </div>
                                <ul className='filter_anime'>
                                    <li className='active'>Day</li>
                                    <li>Week</li>
                                    <li>Month</li>
                                    <li>Years</li>
                                </ul>
                                <div className='filter_anime_gallery'>
                                    {animeItemSidebar.map((anime, index) => <AnimeItemSidebar key={index + 1}/>)}
                                </div>
                            </div>
                            <div className='anime_sidebar_view'>
                                <div className='section-title'>
                                    <h5>
                                        New Comment
                                    </h5>
                                </div>
                                <div className='filter_anime_gallery'>
                                    {animeItemSidebar.map((anime, index) => <AnimeItemCommentSidebar key={index + 1}/>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
}
