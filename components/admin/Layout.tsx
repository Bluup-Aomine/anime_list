import Head from 'next/head'
import React, {useEffect} from 'react'
// Component
import Sidebar from './layout/Sidebar'
import Header from './layout/Header'

export default function Layout({children})
{

    useEffect(() => {
        document.body.classList.add('admin-theme')
    }, [])

    return  <>
        <Head>
            <title>Dashboard Admin !</title>
            {/* GOOGLE FONTS */}
            <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet" />
        </Head>
        <Sidebar/>
        <div id='main'>
            <Header/>
            {children}
        </div>
    </>
}
