type PageHeadingType = {
    title: string,
    subtitle: string
}

export default function PageHeading({title, subtitle}: PageHeadingType) {
    return <div className='page-title'>
        <div className="row">
            <div className='col-12 col-md-6 order-md-1 order-last'>
                <h3>{title}</h3>
                <p className="text-subtitle text-muted">{subtitle}</p>
            </div>
            <div className='col-12 col-md-6 order-md-2 order-first'>
                <nav className='float-start float-lg-end'>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <a href="index.html">Dashboard</a>
                        </li>
                        <li className="breadcrumb-item active" aria-current="page">Form Layout</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
}
