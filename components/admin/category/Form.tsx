import React, {useEffect} from 'react'
import {SubmitHandler, useForm} from 'react-hook-form'
import {Categories} from "../../../utils/type/Category";

type FormValues = {
    name?: string
    slug?: string
    content?: string
}

type FormPropsType = {
    onSubmit: (data) => void
    category: Categories
}

export default function Form({onSubmit, category}: FormPropsType) {
    let config = {}

    if (category) {
        config['defaultValues'] = category
    }
    const {register, handleSubmit, setValue} = useForm<FormValues>()

    useEffect(() => {
        if (category) {
            for (const property in category) {
                // @ts-ignore
                setValue(property, category[property])
            }
        }
    }, [category])

    const _onSubmit: SubmitHandler<FormValues> = data => onSubmit(data)

    return <form action="#" method='POST' className='form form-horizontal' onSubmit={handleSubmit(_onSubmit)}>
        <div className='form-body'>
            <div className='row'>
                <div className='col-md-4'>
                    <label htmlFor="name">Name</label>
                </div>
                <div className='col-md-8 form-group'>
                    <input type="text" id="name" className="form-control" name="name"
                           placeholder="Category name..." {...register('name')}/>
                </div>
                <div className='col-md-4'>
                    <label htmlFor="slug">Slug</label>
                </div>
                <div className='col-md-8 form-group'>
                    <input type="text" id="slug" className="form-control" name="slug"
                           placeholder="Category slug..." {...register('slug')}/>
                </div>
                <div className='col-md-4'>
                    <label htmlFor="content">Content</label>
                </div>
                <div className='col-md-8 form-group'>
                    <textarea className="form-control" name='content' placeholder="Category Content..." id="content"
                              {...register('content')}/>
                </div>
                <div className='col-sm-12 d-flex justify-content-end'>
                    <button type='submit' className='btn btn-primary mb-1'>Submit</button>
                </div>
            </div>
        </div>
    </form>
}
