import React, {useEffect, useState} from 'react'
import {SubmitHandler, useForm} from 'react-hook-form'
import DatePicker from 'react-datepicker'
import {Categories} from '../../../utils/type/Category'
import {DataAnime} from "../../../utils/type/Anime";

type FormValues = {
    title?: string
    slug?: string
    category?: string
    type?: string
    studios?: string
    status?: string
    start_date?: Date
    end_date?: Date
    content?: string
}

type FormPropsType = {
    onSubmit: (data) => void,
    categories: Categories[],
    anime: DataAnime
}

export default function Form({onSubmit, categories, anime = null}: FormPropsType) {
    const [startDate, setStartDate] = useState<Date>(new Date())
    const [endDate, setEndDate] = useState<Date>(new Date())

    let config = {}
    if (anime) {
        config['defaultValues'] = anime
    }
    const {register, handleSubmit, setValue} = useForm<FormValues>(config)

    useEffect(() => {
        register('start_date')
        register('end_date')
    }, [register])

    useEffect(() => {
        if (anime) {
            for (const property in anime) {
                // @ts-ignore
                setValue(property, anime[property])
            }
        }
    }, [anime])

    const _onSubmit: SubmitHandler<FormValues> = data => {
        setValue('start_date', startDate)
        setValue('end_date', endDate)
        console.log(data)
        onSubmit(data)
    }

    return <form action="#" method='POST' className='form form-horizontal' onSubmit={handleSubmit(_onSubmit)}>
        <div className='form-body'>
            <div className='row'>
                <div className='col-md-6 col-12'>
                    <div className='form-group'>
                        <label htmlFor="title">Title</label>
                        <input type="text" id="title" className="form-control" placeholder="Title anime..."
                               name="title" {...register('title')}/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor="title">Slug</label>
                        <input type="text" id="slug" className="form-control" placeholder="Title anime..."
                               name="slug" {...register('slug')}/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor="category">Category</label>
                        <select name="category" id="category" className='form-control' {...register('category')}>
                            {
                                categories && categories.map((category: Categories) => <option
                                    value={category.slug}>{category.name}</option>)
                            }
                        </select>
                    </div>
                </div>
                <div className='col-md-6 col-12'>
                    <div className='form-group'>
                        <label htmlFor="type">Type</label>
                        <input type="text" id="type" className="form-control" placeholder="Type anime..."
                               name="type" {...register('type')}/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor="studios">Studios</label>
                        <input type="text" id="studios" className="form-control" placeholder="Studios anime..."
                               name="studios" {...register('studios')}/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor="status">Status</label>
                        <select name="status" id="status" className='form-control' {...register('status')}>
                            <option value="end">End</option>
                            <option value="progress">Progress</option>
                        </select>
                    </div>
                </div>
                <div className='col-md-6 col-12'>
                    <div className='form-group'>
                        <label htmlFor="start_date">Start date</label>
                        <DatePicker selected={startDate} onChange={(date: Date) => setStartDate(date)}
                                    className='form-control'/>
                    </div>
                </div>
                <div className='col-md-6 col-12'>
                    <div className='form-group'>
                        <label htmlFor="end_date">End date</label>
                        <DatePicker selected={endDate} onChange={(date: Date) => setEndDate(date)}
                                    className='form-control'/>
                    </div>
                </div>
                <div className='col-md-12 col-12'>
                    <div className='form-group'>
                        <label htmlFor="content">Content</label>
                        <textarea name="content" id="content" className='form-control'
                                  placeholder='Content anime...' {...register('content')}/>
                    </div>
                </div>
                <div className="col-12 d-flex justify-content-end">
                    <button type="submit" className="btn btn-primary mb-1">Submit</button>
                </div>
            </div>
        </div>
    </form>
}
