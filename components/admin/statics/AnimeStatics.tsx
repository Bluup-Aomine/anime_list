import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEye} from '@fortawesome/free-solid-svg-icons'

export default function AnimeStatics()
{
    return <div className='col-6 col-lg-3 col-md-6'>
        <div className='card'>
            <div className='card-body py-4-5'>
                <div className='row'>
                    <div className='col-md-4'>
                        <div className='stats-icon purple'>
                            <FontAwesomeIcon icon={faEye} />
                        </div>
                    </div>
                    <div className='col-md-8'>
                        <h6 className="text-muted font-semibold">Animes</h6>
                        <h6 className="font-extrabold mb-0">112.000</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
