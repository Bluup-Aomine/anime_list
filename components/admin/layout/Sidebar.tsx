import { BiGridAlt } from 'react-icons/bi'
import SubMenuSidebar from "./SubMenuSidebar";

export default function Sidebar()
{
    return <div className='active' id='sidebar'>
        <div className='sidebar-wrapper'>
            <div className='sidebar-header'>
                <div className='d-flex justify-content-between'>
                    <div className='sidebar-header-logo'>
                        <a href="#">
                            <img src="/admin/header_admin_logo.png" alt="Logo"/>
                        </a>
                    </div>
                    <div className='sidebar_header_toggler'>
                        <a href="#" className='d-xl-none d-block'>
                            <i className='bi bi-x bi-middle'/>
                        </a>
                    </div>
                </div>
            </div>
            <div className='sidebar-menu'>
                <ul>
                    <li className="sidebar-title">Menu</li>
                    <li className="sidebar-item active">
                        <a href="#" className="sidebar-link">
                            <BiGridAlt/>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <SubMenuSidebar name='Animes' tagUri='anime'/>
                    <SubMenuSidebar name='Categories' tagUri='category'/>
                    <SubMenuSidebar name='Tags' tagUri='tag'/>
                </ul>
            </div>
        </div>
    </div>
}
