import Link from 'next/link'

import {BiGridAlt} from "react-icons/bi";
import {useCallback, useState} from "react";

export default function SubMenuSidebar({name, tagUri}) {
    const [active, setActive] = useState<Boolean>(false)

    const handleSubmit = useCallback(() => {
        setActive(!active)
    }, [active])

    return <li className='sidebar-item has-sub'>
        <a href="#" className="sidebar-link" onClick={handleSubmit}>
            <BiGridAlt/>
            <span>{name}</span>
        </a>
        <ul className={`submenu ${active && 'd-block'}`}>
            <li className='submenu-item'>
               <Link href={`/admin/${tagUri}`}>
                   <a>Listening</a>
               </Link>
            </li>
            <li className='submenu-item'>
                <Link href={`/admin/${tagUri}/create`}>
                    <a>Create</a>
                </Link>
            </li>
        </ul>
    </li>
}
