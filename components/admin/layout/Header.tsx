import { BiAlignJustify } from 'react-icons/bi'

export default function Header()
{
    return <header className='mb-3'>
        <a href="#" className='d-block d-xl-none'>
            <BiAlignJustify/>
        </a>
    </header>
}
