import {useEffect, useState} from 'react'
import dynamic from 'next/dynamic'
import {DocumentContext} from 'next/document'

const mapLayoutToComponent = {
    public: dynamic(import('./../components/Layout')),
    admin: dynamic(import('./../components/admin/Layout'))
}

const Layout = ({uriRoute, children}) => {
    const [layoutState, setLayoutState] = useState('public')
    const Component = mapLayoutToComponent[layoutState]

    useEffect(() => {
        uriRoute.include('admin') && setLayoutState('admin')
    }, [layoutState])

    return <Component>{children}</Component>
}

Layout.getInitialProps = async ({req}: DocumentContext) => {
    const uriRoute = req.url

    return {uriRoute}
}

export default Layout
