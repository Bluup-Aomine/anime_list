import React from 'react'
// FontAwesome

export default function Footer() {
    return <footer className='footer'>
        <div className='container'>
            <div className='row'>
                <div className='col-lg-3'>
                    <div className='footer_logo'>
                        <a href="#">
                            <img src="/header_logo.png" alt=""/>
                        </a>
                    </div>
                </div>
                <div className='col-lg-6'>
                    <div className='footer_nav'>
                        <ul>
                            <li className='active'>
                                <a href="#">Homepage</a>
                            </li>
                            <li>
                                <a href="#">Categories</a>
                            </li>
                            <li>
                                <a href="#">Animes</a>
                            </li>
                            <li>
                                <a href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className='col-lg-3'>
                    <p>
                        Copyright © 2021 2021 All rights reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>
}
