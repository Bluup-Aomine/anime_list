const HeaderLogo = () => {
    return <div className='header_logo'>
        <a href="#">
            <img src='/header_logo.png' alt='Header logo'/>
        </a>
    </div>
}

const HeaderNav = () => {
    return <div className='header_nav'>
        <nav className='header_menu menu_mobile'>
            <ul>
                <li className='active'>
                    <a href="#">HomePage</a>
                </li>
                <li>
                    <a href="#">Animes</a>
                </li>
                <li>
                    <a href="#">
                        Categories
                        <span className='arrow_carrot-down'/>
                    </a>
                    <ul className='dropdown'>
                        <li>
                            <a href="#">Categories</a>
                        </li>
                        <li>
                            <a href="#">Categories</a>
                        </li>
                        <li>
                            <a href="#">Categories</a>
                        </li>
                        <li>
                            <a href="#">Categories</a>
                        </li>
                        <li>
                            <a href="#">Categories</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
}

export default function Header() {
    return <header className='header'>
        <div className='container'>
            <div className='row'>
                <div className='col-lg-2'>
                    <HeaderLogo/>
                </div>
                <div className='col-lg-8'>
                    <HeaderNav/>
                </div>
                <div className='col-lg-2'>
                    <div className='header_right'>
                        <a href="#" className='search'>
                            <span className='icon_search'/>
                        </a>
                        <a href="#">
                            <span className='icon_profile'/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
}
