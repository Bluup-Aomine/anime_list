// FontAwesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons'

export default function PaginationAnime()
{
    return <div className='anime_pagination'>
        <a href="#" className='current-page'>1</a>
        <a href="#">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">
            <FontAwesomeIcon icon={faAngleDoubleRight}/>
        </a>
    </div>
}
