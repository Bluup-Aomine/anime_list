import React from "react";

export default function AnimeReview() {
    return <div className='anime_review_item'>
        <div className='anime_review_item_bg'>
            <img src="/review-1.jpg" alt="Logo user"/>
        </div>
        <div className='anime_review_item_text'>
            <h6>
                Chris Curry -
                <span>1 hour ago</span>
            </h6>
            <p>
                whachikan Just noticed that someone categorized this as belonging to the genre
                "demons" LOL
            </p>
        </div>
    </div>
}
