import React from 'react'
// FontAwesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faLocationArrow} from '@fortawesome/free-solid-svg-icons'

export default function AnimeReviewForm() {
    return <form action="#" method='POST'>
        <textarea placeholder="Your Comment"/>
        <button type='submit'>
            <FontAwesomeIcon icon={faLocationArrow}/> Review
        </button>
    </form>
}
