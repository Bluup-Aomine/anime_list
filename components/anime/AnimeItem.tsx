import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faComments, faStar} from '@fortawesome/free-solid-svg-icons'
import React from 'react'

export function AnimeItem() {
    return <div className='col-lg-4 col-md-6 col-sm-6'>
        <div className='anime_list'>
            <div className='anime_item_bg set_bg_img' style={{backgroundImage: `url('/trend-1.jpg')`}}>
                <div className='comment'>
                    <FontAwesomeIcon icon={faComments}/>
                    15
                </div>
                <div className='like'>
                    <FontAwesomeIcon icon={faStar} />
                    8
                </div>
            </div>
            <div className='anime_item_text'>
                <ul>
                    <li>Active</li>
                    <li>Movie</li>
                </ul>
                <h5>
                    <a href="#">
                        Boruto: Naruto Next Generations
                    </a>
                </h5>
            </div>
        </div>
    </div>
}

export function AnimeItemSidebar()
{
    return <div className='anime_sidebar_view_item set_bg_img' style={{backgroundImage: `url('/tv-1.jpg')`}}>
        <div className='like'>
            <FontAwesomeIcon icon={faStar}/>
            10
        </div>
        <h5>
            <a href="#">Boruto: Naruto next generations</a>
        </h5>
    </div>
}

export function AnimeItemCommentSidebar() {
    return <div className='anime_sidebar_comment_item'>
        <div className='anime_sidebar_comment_item_bg'>
            <img src="/comment-1.jpg" alt="Image comment item anime"/>
        </div>
        <div className='anime_category_comment_item_text'>
            <ul>
                <li>Active</li>
                <li>Movie</li>
            </ul>
            <h5>
                <a href="#">The Seven Deadly Sins: Wrath of the Gods</a>
            </h5>
        </div>
    </div>
}
