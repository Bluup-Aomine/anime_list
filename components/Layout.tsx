import React, {useEffect} from 'react'
import Head from 'next/head'
// Component
import Header from './layout/Header'
import Footer from './layout/Footer'

export default function Layout({children})
{
    useEffect(() => {
        document.body.classList.add('dark-theme')
    }, [])

    return <>
        <Head>
            <title>Welcome on our site !</title>
            {/* GOOGLE FONTS */}
            <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet"/>
            <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet"/>
        </Head>
        <Header/>
        {children}
        {/* Anime Footer */}
        <Footer/>
    </>
}
