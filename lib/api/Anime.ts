import axios from 'axios'
// Types
import {DataCategoryForm} from '../../utils/type/Data'

/**
 * @property {string} apiUri
 */
export default class Category
{
    private readonly apiUri: string

    constructor() {
        this.apiUri = 'http://127.0.0.1:3333/api'
    }

    public async getAnimes() {
        return axios.get(`${this.apiUri}/animes`)
    }

    /**
     * @param {number} id
     */
    public async getAnime(id: number) {
        return axios.get(`${this.apiUri}/anime/${id}`)
    }

    /**
     * @param {DataCategoryForm} data
     */
    public async createAnime(data: DataCategoryForm)
    {
        return axios.post(`${this.apiUri}/anime/create`, data)
    }

    /**
     * @param {DataCategoryForm} data
     * @param {number} id
     */
    public async updateAnime(data: DataCategoryForm, id: number)
    {
        return axios.put(`${this.apiUri}/anime/edit/${id}`, data)
    }
}
