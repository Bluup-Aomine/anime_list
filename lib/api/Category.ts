import axios from 'axios'
// Types
import {DataCategoryForm} from '../../utils/type/Data'

/**
 * @property {string} apiUri
 */
export default class Category
{
    private readonly apiUri: string

    constructor() {
        this.apiUri = 'http://127.0.0.1:3333/api'
    }

    public async getCategories() {
        return axios.get(`${this.apiUri}/categories`)
    }

    /**
     * @param {number} id
     */
    public async getCategory(id: number) {
        return axios.get(`${this.apiUri}/category/${id}`)
    }

    /**
     * @param {DataCategoryForm} data
     */
    public async createCategory(data: DataCategoryForm)
    {
        return axios.post(`${this.apiUri}/category/create`, data)
    }

    /**
     * @param {DataCategoryForm} data
     * @param {number} id
     */
    public async updateCategory(data: DataCategoryForm, id: number)
    {
        return axios.put(`${this.apiUri}/category/edit/${id}`, data)
    }
}
